# HappyLinuxPizza

![alt text][logo]

The HappyLinuxPizza site made with [Joomla](https://www.joomla.org/)

## Requirements
- docker
- docker-composer

## Docker Commands [Linux]
- docker-compose up - Starts containers
- docker exec -it [container-id] bash - Enter container userspace

[logo]: linuxpizza.jpeg "Linux Pizza Mascot"


## Wanted features
Everyone should be able to create a user, and verify it by email.

Every submitted game should be reviewed by staff (admins, moderators)
Users should be able to rate, comment and submit games.

Every gamepage should contain the following information:
 - Title of the game
 - Developer(s)
 - First release date and current versions release date
 - A short description of the game
 - Multiplayer? Yes/No
 - Genre
 - 2D/3D
 - Terminal or Graphical
 - How much resources needed
 - The language the game is written in
 - License of the software
 - Some screenshots and/or videos ingame
 - RSS feed from the original creators webpage - if any
 

Site should have a sponsors page.

Site should have a "In collaboration with" site - yes we do have some potential collaborators on other projects :)

Site should have a maintainers page with contact information



Will add more when brain works better




